#include <systemc.h>
#include "sign_ext.hpp"

using namespace std;
using namespace sc_core;

void sign_ext::behav()
{
    while(true)
    {
		wait(); 
    	out->write(in->read());
    }
}
