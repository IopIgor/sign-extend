#ifndef sign_ext_HPP
#define sign_ext_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(sign_ext) 
{
  static const unsigned bit_in = 16;
  static const unsigned bit_out = 32;

  sc_in<sc_bv<bit_in> > in;
  sc_out<sc_bv<bit_out> > out;
  
  SC_CTOR(sign_ext) 
  {
    SC_THREAD(behav);
        sensitive << in;
  } 
  
 private:
  void behav();
};

#endif
