#include <systemc.h>
#include <iostream>
#include "sign_ext.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(1, SC_NS);
const unsigned bit_in = 16;
const unsigned bit_out = 32;

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<bit_in> > in;
  sc_signal<sc_bv<bit_out> > out;

  sign_ext sign_ext_test;

  SC_CTOR(TestBench) : sign_ext_test("sign_ext_test")
  {
    SC_THREAD(stimulus_thread);
    SC_THREAD(check_and_watcher_thread);
      sensitive << in;

    sign_ext_test.in(this->in);
    sign_ext_test.out(this->out);
  }

  bool Check() const {return error;}
    
 private:
  bool error;

  void stimulus_thread() 
  {
    in.write(5);
    wait(wait_time);

    in.write(0);
    wait(wait_time);

    in.write(12);
    wait(wait_time);
  }

 void check_and_watcher_thread() 
  {
    error = 0;
    while(true)
    {
    	wait();
    	wait(1, SC_PS);
    	if(in.read().to_uint() != out.read().to_uint())
      {
        cout << "test failed!!" << endl;
        error = 1;
      }

	    cout << "At time " << sc_time_stamp() << ":\nsignal " << in.read() 
           << " extended to 32 bit as " << out.read() << endl; 
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.Check();
}

